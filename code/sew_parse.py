import os
from config import *
from lxml import etree
path = '/Users/mark/sew_conservative'

record_file = open('record-1.txt','r')
files = record_file.readlines()
processed_files = []
for file_name in files:
    if file_name.startswith('wiki'):
        # Because this file also contains exception file‘s information
        processed_files.append(file_name.strip())
#print(processed_files)

i = 0
output_path = '../resources/sew_output.txt'
output_file = open(output_path,'w')

file_num = 0
for direct in os.listdir(path):
    # Because the sew dataset is too huge, each time I only process a small portion of it. 
    # Keep the processed directories in a file
    # Skip these processed file in the next round
    if direct in processed_files:
        print('ignore ',direct)
        continue
    file_num += 1
    if not direct.startswith('wiki'):
        continue
    print(direct)
    
    
    for file in os.listdir(path+'/' + direct):
        if not file.endswith('xml'):
            continue
        context = etree.iterparse(path+'/'+direct+'/'+file)
        
        i = 0
        tag =''
        
        # XMLSyntaxError： can't parsed encoding
        try:
            for action, elem in context:
                i += 1
                attributes = elem.attrib
                
                if i % 5000000 == 0:
                    print('processing line: ', i)
                
                # extract input sentence
                if elem.tag == 'text':
                    # add full file path to debug easily, I can find the exception file easily
                    output_file.write(path+'/'+direct+'/'+file + '||||'+ str(elem.text))
                
                # extract BabelNet synset
                
                if elem.tag == 'babelNetID':
                    tag = tag + elem.text
                if elem.tag == 'mention':
                    tag = tag + "*&#$##$#&*" + elem.text
                if elem.tag == 'anchorStart':
                    tag = tag + "*&#$##$#&*" + elem.text
                if elem.tag == 'anchorEnd':
                    tag = tag + "*&#$##$#&*" + elem.text
                    output_file.write(str(tag) + '\n')
                    tag = ''
                
                elem.clear()
                while elem.getprevious() is not None:
                    del elem.getparent()[0]
        except:
            print(path+'/'+direct+'/'+file)
    output_file.flush()
    if file_num == 50:
        break
output_file.close()
print('Finish the processing now!!')

