import evaluation 
import synset_evaluation
from preprocessing import load_mapping_file
from config import MAPPING_FILE_PATH,EMBEDDING_PATH,Merged_EMBEDDING_PATH,SYNSET_EMBEDDING_PATH
import nltk
from nltk.corpus import wordnet as wn


# Merge word sense embedding and synset embedding
if __name__ == "__main__":
    
    # load data
    Babel_to_Synset_mapping_dict = load_mapping_file(MAPPING_FILE_PATH)
    synset_word_vectors,synset_word2idx,synset_word2lemmaId = synset_evaluation.load_data(SYNSET_EMBEDDING_PATH)
    word_vectors,word2idx,word2lemmaId = evaluation.load_data(EMBEDDING_PATH)
    
    # build a whitelist, we only add words which are not in the whitelist
    whitelist = list(word2idx.keys())
    
    print('Begin extract all the complement embeddings...')
    result = {}
    i = 0
    # find the BabelNetId which starts with 'bn',get the corresponding lemmas
    # if lemmas is not in whitelist, add to result
    for key, value in synset_word2idx.items():
        if key.startswith('bn:'):
            
            if(not key in Babel_to_Synset_mapping_dict.keys()):
                continue
            
            synset_id = Babel_to_Synset_mapping_dict[key]
            synset = wn.synset_from_pos_and_oﬀset(synset_id[-1], int(synset_id[:-1]))
            
            for lemma in synset.lemma_names():
                item = lemma + '_' + key
                if((not item in whitelist) and (not lemma in whitelist)):
                    result[item] = list(synset_word_vectors[value[0]])
    print('Extract all the complement embeddings, the number of complement embeddings is ', len(result))
    print('----------------------------')
    
    # merge two embeddings
    print('Begin to merge the embedding')
    original_file = open(EMBEDDING_PATH)
    data = original_file.readlines()
    original_file.close()
    
    # update the statistics information
    number,dim = data[0].split(' ')
    
    total_number = int(number) + len(result)
    data[0] = str(total_number) + ' ' + dim
    
    # the path for merged_embedding
    output_file = open(Merged_EMBEDDING_PATH,'w')
    output_file.writelines(data)
    
    for key, value in result.items():
        output_value = ''
        for num in value:
            
            output_value = output_value + ' ' + str(num)

        output_file.write(key  + output_value + '\n')
    output_file.close()
    
    print('Finish the merging procedure')
    print('----------------------------')
    
    # Check the number of embedding and the dimention of each embedding
    print('Begin to check the number of embedding and the dimention of each embedding...')
    merge_file = open(Merged_EMBEDDING_PATH)
    data = merge_file.readlines()
    number,dim = data[0].split(' ')
    assert int(number) == len(data) - 1
    for i in range(1,len(data)):
        try:
            assert len(data[i].split(' ')) == int(dim) + 1
        except:
            print(data[i].split(' ')[:5])
    print('Finish checking!')
    print('----------------------------')