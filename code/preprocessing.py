#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon May  6 17:06:54 2019

@author: Kai Niu
"""

from config import *
from lxml import etree
import csv
import gzip

def extract(input_path, output_path):
    '''
    Purpose：
    1.extract target sentence
    2.extract potential word sense from XML file saving in the tag#lemma_BabelnetID form
    
    Parameters：
        input_path：the input_path for the required parsing file
        output_path: I store the parse result in an intermediate file 
            Form：
                Sentence1
                anchor###lemma_BabelnetID1
                anchor###lemma_BabelnetID2
                .
                .
                Sentence2
                anchor###lemma_BabelnetID1
                anchor###lemma_BabelnetID2
    '''
    output_file = open(output_path,'w')
    dd = gzip.GzipFile(input_path)
    context = etree.iterparse(dd)
    
    i = 0
    for action, elem in context:
        attributes = elem.attrib
        print(elem.text)
        
        if i % 5000000 == 0:
            print('processing line: ', i)
        
        # extract input sentence
        if elem.tag == 'text' and 'lang' in attributes and attributes['lang'] =='en':
            output_file.write(str(elem.text) + '\n')
        
        # extract BabelNet synset
        if elem.tag == 'annotation' and 'lang' in attributes and attributes['lang'] =='en':
            output_file.write(str(attributes['anchor'])+ '###'+ str(attributes['lemma']) + '_' + str(elem.text) + '\n' )
        
        # release the memory
        elem.clear()
        while elem.getprevious() is not None:
            del elem.getparent()[0]
    output_file.close()
    print('Finish the processing now!!')


def preprocessing(input_path, mapping_dict, output_path):
    '''
    Purpose：
            1.Process the intermediate result generating from extract function
            2.Filter out the lemma_BabelnetId which doesn't exist corresponding mapping
            3.Substitute the target words with anchor#lemma_BabelnetId
    Parameters：
            input_path: The intermediate result file path
            mapping_dict: A id mapping dictionary
            output_path: The path for storing the processed file
    '''
    i = 0
    output_file = open(output_path,'w')
    output_csv = csv.writer(output_file)
    
    with open(input_path, 'r',encoding='utf-8') as f:
        lines = f.readlines()
        n = len(lines)
        while(i < n):
            data = lines[i]
            original_data = data.strip()
            datas = original_data.split('###')
                                        
            # This is our target input sentence, we should replace the orignal words with lemma_BabelnetId
            if len(datas) == 1 and i + 1 < n:
                
                # In case that we don't need to process this sentence, we can't motify i directly
                tag_index = i + 1
                tag_data = lines[tag_index]
                tag_original_data = tag_data.strip()
                tag_datas = tag_original_data.split('###')
                
                tokens = original_data.split(' ')
                tokens_length = len(tokens)
                index = 0
                
                # This means that this original_data is the substitution pairs
                while(len(tag_datas) == 2):
                   
                    # Check there exists a corresponding WordNet id
                    # Note: many lemma contains "_", so we should handle this case
                    try:
                        temp_list = tag_datas[1].split('_')
                        if len(temp_list) > 2:
                            lemma = '_'.join(temp_list[:-1])
                            BabelnetId = temp_list[-1]
                        else:
                            lemma, BabelnetId = temp_list[0],temp_list[1]
                    except:
                        print(tag_datas[1])
                    
                    # we are only interested in word sence
                    if len(BabelnetId.split(' ')) > 1:
                        continue
                    
                    # in order to reduce the case: 'bank bank' the first one and the second one has different word sence，we can't use replace function directly
                    # we surely can find a token to substitute
                    if BabelnetId in mapping_dict:
                        while index < tokens_length:
                            old = tokens[index]
                            tokens[index] = tokens[index].replace(tag_datas[0],tag_datas[1])
                            if old != tokens[index]:
                                index += 1
                                break
                            else:
                                index += 1
                    i = tag_index
                    tag_index = tag_index + 1
                    if tag_index >= n:
                        break
                    tag_data = lines[tag_index]
                    tag_original_data = tag_data.strip()
                    tag_datas = tag_original_data.split('###')
            output_csv.writerow([' '.join(tokens)])
            i = i+1
    output_file.close()
    print('close the output_file now!')
                        

              
def load_mapping_file(input_path):
    '''
    Purpose：
        1.Load the mapping pair from BabelNet id to Synset id
    '''
    mapping_dict = {}
    i = 0
    with open(input_path, 'r',encoding='utf-8') as f:
        for data in f:
            i += 1
            original_data = data.strip()
            datas = original_data.split()
            if len(datas) == 1:
                print('lien 1 ', original_data)
            if len(datas) == 2:
                mapping_dict[datas[0]] = datas[1]
            elif len(datas) > 2:
                mapping_dict[datas[0]] = datas[1]
                print(original_data)

    assert i == len(mapping_dict)
    return mapping_dict

if __name__ == "__main__":
#     path = "../resources/sew_conservative.tar.gz"
#     extract(path,OUTPUT_PATH)
    mapping = load_mapping_file(MAPPING_FILE_PATH)
#    synset_preprocessing(OUTPUT_PATH,mapping,SYNSET_DATA_PATH)