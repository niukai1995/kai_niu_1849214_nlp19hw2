#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed May  8 12:05:49 2019

@author: mark
"""
from config import *
import numpy as np
from scipy import spatial,stats
from sklearn.metrics.pairwise import cosine_similarity

# load the trained embeddings
def load_data(path):
    """
    Purpose：
        1.Load and return trained embedding
        2.Return a mapping from word to a list containing all the possible word_BabelNetId
    Parameters：
        path: The Embedding path
    Return：
        1.word2idx：
            type: ndarray
            content: a mapping from word to a unique Integer
        2.word_vectors：
            type: dict
            content: a mapping from Integer to the corresponding word Embedding vector
        3.word2lemmaId: 
            type: dict
            content: a mapping from word to a list containing all the possible word_BabelNetId
    """
    with open(path, 'r',encoding='utf-8') as f:
        line = f.readline().strip().split(" ")
        N, dim = map(int, line)
        print(N,dim)
        word_vectors = []
        
        # Add special symbol embedding, and give them a random value between [-1 , + 1]
        word2idx = {}
        word2lemmaId = {}


        idx = 0
        for k in range(N):
            sentence = f.readline()
            line = sentence.strip().split(" ")
            
            # check whether the dimension of each word is correct
            if len(line) != dim + 1:
                continue
            
            # build a dictionary, word : [word_BabelnetID]
            word_BabelnetID = line[0]
            
            # check whther it is a word sense
            bn_tag = word_BabelnetID.split('_')[-1]
            if bn_tag.startswith('bn:'):
                word = '_'.join(word_BabelnetID.split('_')[:-1])
                word2idx.setdefault(word, []).append(idx)
                word2lemmaId.setdefault(word, []).append(word_BabelnetID)
            else:
                word2idx.setdefault(word_BabelnetID, []).append(idx)
                word2lemmaId.setdefault(word_BabelnetID, []).append(word_BabelnetID)
                
            # make sure that the word embedding is added in every round
            vector = np.asarray(list(map(float, line[1:])), dtype=np.float32)
            word_vectors.append(vector)
            idx += 1


        print( 'Vocab size:', len(word_vectors))
        print( 'word2idx:', len(word2idx))
        print( 'index:', idx)

        word_vectors = np.array(word_vectors, dtype=np.float32)
    return word_vectors,word2idx,word2lemmaId



def score(test_path, word_vectors, word2idx, word2lemmaId):
    """
    Parameters：
        test_path: The path of WordSimilarity-353 file
        word_vectors: The ndarray object storing all the embedding information
        word2idx: A dictionary mapping from word to Integer
        word2lemmaId：A dictionary mapping from word to a list containing all the possible word_BabelNetId
    Return：
        1.glod_scores
            type: list
            content: the integer extracted from WordSimilarity-353 file
        2.predict_scores
            type: list
            content: the cosine similarity scores integer
        3.score_value: 
            type: int
            content: Spearman correlation between gold similarity scores and cosine similarity scores
    """

    glod_scores = []
    predict_scores = []
    unfind_num = 0
    total_num = 0
    with open(test_path, 'r',encoding='utf-8') as f:
        test_data = f.readlines()[:]
        total_num = len(test_data) - 1
        for line in test_data[1:]:
            data = line.split('\t')
            if len(data) != 3:
                print(len(data))
                continue
            if not (data[0] in word2idx.keys() and data[1] in word2idx.keys()):
                print(data)
                unfind_num += 1
                continue
            word_set1 = word2idx[data[0]]
            word_set2 = word2idx[data[1]]
            
            pred_score = -1
            for word1 in word_set1:
                for word2 in word_set2:
                    word_vec1 = word_vectors[word1].reshape(1,-1)
                    word_vec2 = word_vectors[word2].reshape(1,-1)

                    cos_lib = cosine_similarity(word_vec1,word_vec2)[0][0]
                    result = 1-spatial.distance.cosine(word_vectors[word1], word_vectors[word2])
                    pred_score = max(pred_score, result)
                    
            # collect scores
            glod_scores.append(float(data[2]))
            predict_scores.append(pred_score)
    
    print('total num ', total_num)
    print('ufind_num ', unfind_num)
    print('finish the score calculation!')
    
    score_value = stats.spearmanr(glod_scores,predict_scores)
    print('finial score is ', score_value)
    
    return glod_scores,predict_scores,score_value
    

if __name__ == "__main__":
    word_vectors,word2idx, word2lemmaId =load_data(EMBEDDING_PATH)
#    word_synset_mapping = load_word_synset_mapping(OUTPUT_PATH)
##    glod_scores,predict_scores,score_value = score(EVALUATE_PATH,word_vectors, word2idx,word2lemmaId)
#    glod_scores,predict_scores = synset_score(EVALUATE_PATH,word_vectors, word2idx,word2lemmaId,word_synset_mapping)