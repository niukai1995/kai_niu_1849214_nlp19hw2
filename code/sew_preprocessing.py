#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon May 20 13:29:10 2019

@author: mark
"""
from config import *
from lxml import etree
import csv
#from bz2file import 
import gzip

def load_mapping_file(input_path):
    '''
    Purpose：
        1.Load the mapping pair from BabelNet id to Synset id
    '''
    mapping_dict = {}
    i = 0
    with open(input_path, 'r',encoding='utf-8') as f:
        for data in f:
            i += 1
            original_data = data.strip()
            datas = original_data.split()
            if len(datas) == 1:
                print('lien 1 ', original_data)
            if len(datas) == 2:
                mapping_dict[datas[0]] = datas[1]
            elif len(datas) > 2:
                mapping_dict[datas[0]] = datas[1]
                print(original_data)

    assert i == len(mapping_dict)
    return mapping_dict

def preprocessing(input_path, mapping_dict, output_path):
    '''
    Purpose：
            1.Process the intermediate result generating from extract function
            2.Filter out the lemma_BabelnetId which doesn't exist corresponding mapping
            3.Substitute the target words with anchor#lemma_BabelnetId
    Parameters：
            input_path: The intermediate result file path
            mapping_dict: A id mapping dictionary
            output_path: The path for storing the processed file
    '''
    i = 0
    output_file = open(output_path,'w')
    output_csv = csv.writer(output_file)
    error_file = ''
    with open(input_path, 'r',encoding='utf-8') as f:
        lines = f.readlines()
        n = len(lines)
        while(i < n):
            data = lines[i]
            # I met bug here, I should using very complex symbols to saparate file_path, lemma, Index_Start, Index_End
            # Remove the file path information here, if we meet error, output the file path
            try:
                file_name,original_data = data.strip().split('||||')
            except:
                print(data)
            datas = original_data.split('*&#$##$#&*')
            
            # This is our target input sentence, we should substitute the orignal words with lemma_BabelnetId
            while len(datas) == 1 and i + 1 < n:
                
                tag_index = i + 1
                tag_data = lines[tag_index]
                tag_original_data = tag_data.strip()
                datas = tag_original_data.split('*&#$##$#&*')
                if len(tag_data.strip().split('||||')) == 2:
                    break
                elif len(datas) == 1:
                    original_data += '  ' + tag_original_data
                    i = tag_index
#            print(original_data)
#            print('---------------------------------')
                
            if i + 1 < n:
                # In case that we don't need to process this sentence, we can't motify i directly
                tag_index = i + 1
                tag_data = lines[tag_index]
                tag_original_data = tag_data.strip()
                tag_datas = tag_original_data.split('*&#$##$#&*')
                
                tokens = original_data.split(' ')
#                print(len(tokens))
                tokens_length = len(tokens)
#                replaced_tokens_length = tokens_length
                
                # This means that this line is the target we want to do substitution
                while(len(tag_datas) == 4):
                    BabelnetId = tag_datas[0]
                    lemma = tag_datas[1]
                    start = tag_datas[2]
                    
#                    print(lemma)
                    
                    # we are only interested in word sence
                    if len(lemma.split(' ')) > 1:
                        i = tag_index
                        tag_index = tag_index + 1
                        if tag_index >= n:
                            break
                        else:
                            tag_data = lines[tag_index]
                            tag_original_data = tag_data.strip()
                            tag_datas = tag_original_data.split('*&#$##$#&*')
                            continue
                    
                    # in order to reduce the case: 'bank bank' the first one and the second one has different word sence
                    # we surely can find a token to substitute
                    if BabelnetId in mapping_dict:
                            try:
#                                print(tokens[int(start)]+' 1')
                                assert tokens[int(start)] == lemma
                                tokens[int(start)] = lemma+'_'+ str(BabelnetId)
                                
                            except:
#                                print(start + '_' + lemma+'_'+ str(BabelnetId))
                                if error_file != file_name:
                                    print('error ',file_name)
                                    file_name = error_file
                    i = tag_index
                    tag_index = tag_index + 1
                    if tag_index >= n:
                        break
                    tag_data = lines[tag_index]
                    tag_original_data = tag_data.strip()
                    tag_datas = tag_original_data.split('*&#$##$#&*')
            output_csv.writerow([' '.join(tokens)])
            i = i+1
    output_file.close()
    print('close the output_file now!')
    
if __name__ == "__main__":
    path = "../resources/sew_output.txt"
#    path = "../resources/test.txt"
#    path = '/Users/mark/sew_input.txt'
#     extract(path,OUTPUT_PATH)
    mapping = load_mapping_file(MAPPING_FILE_PATH)
    preprocessing(path,mapping,'../resources/sew_data.csv')