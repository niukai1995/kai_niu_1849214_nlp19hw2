#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed May 22 10:59:12 2019

@author: Kai Niu
"""

from config import *
from lxml import etree
import csv
import pandas as pd
import gzip

def synset_preprocessing(input_path, output_path):
    '''
    Parameters:
        input_path
    '''
    i = 0
    output_file = open(output_path,'w')
    output_csv = csv.writer(output_file)
    
    data = pd.read_csv(input_path, header=None)
    
    for sentence in data[0]:
        tokens = sentence.split(' ')
        for index in range(len(tokens)):
            characters = tokens[index].split('_')
            if characters[-1].startswith('bn:'):
                tokens[index] = '_'.join(characters[:-1])
        
        output_csv.writerow([' '.join(tokens)])
        i = i+1
        if i % 100000 == 0:
            print('preprocessing ', i)
    output_file.close()
    print('close the output_file now!')
              

if __name__ == "__main__":
#     path = "../resources/sew_conservative.tar.gz"
#     extract(path,OUTPUT_PATH)
    synset_preprocessing(OUTPUT_PATH,SYNSET_DATA_PATH)