#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon May  6 17:17:13 2019

@author: Kai Niu
"""
INPUT_PATH = '../resources/EuroSense/eurosense.v1.0.high-i.xml'
#INPUT_PATH = '../resources/test.xml'
OUTPUT_PATH = '../resources/EuroSense/input.txt'
#OUTPUT_PATH = '../resources/EuroSense/mini_input.txt'
MAPPING_FILE_PATH = '../resources/bn2wn_mapping.txt'
DATA_PATH = '../resources/data.csv'
SYNSET_DATA_PATH = '../resources/synset_data.csv'

#EMBEDDING_PATH = '../resources/embeddings_2.vec'
#EMBEDDING_PATH = 'merge.vec'
EMBEDDING_PATH = 'capital_mincount_30_merged1_2_3_1_2_4_Model_mincount30_300_10_500_epech5/embeddings_2.vec'
SYNSET_EMBEDDING_PATH = '../resources/synset_embeddings_500.vec'
Merged_EMBEDDING_PATH  = '../resources/merged.vec'

EVALUATE_PATH = '../resources/wordsim353/combined.tab'
#EVALUATE_PATH = '../resources/wordsim353/test.txt'