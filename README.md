The structure of my Project.
```
|-- code  Directory

|	-- config.py
|	-- model.py 
|	-- evaluation.py

    Sew Dataset
|	-- sew_parse.py
|	-- sew_preprocessing.py

    Synset Sense Embedding
|	-- synset_preprocessing.py
|	-- synset_evaluation.py

    Analysis
|	-- synset_evaluation.py

|-- resources  Directory
|	-- embeddings.vec

```